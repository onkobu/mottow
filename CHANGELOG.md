# 1.0.3
## Static web resources

Goal: fill webroot of server and deliver an index.html with further deep links

* with CSS
* existing GET of transportations
* compatible with Java module API (opens accordingly)
* does not expose Class path

## Testimonials

Goal: have some fun with hard to disprove opinions that everybody uses on the internet

## Database setup

* schema recorded in separate table
* shell script to avoid re-issuing ALTER TABLE since SQLite has no IF NOT EXISTS in ALTER statements
* user credential column in user_profile
* dropped unnecessary full setup SQL file

## PBKDF2

Goal: have a login and transfer a principal to user-related use cases

* authenticator service comparing PDKBF2 credentials
* unit tests asserting cross functionality with GRUB password script
* serve user profile only to authenticated users and only their own profile

## Switch to jdeps

* Undertow is not a Java module yet (2022!)
* dependencies of Undertow from JBoss/ Wildfly are neither Java modules
* added a pom.xml to fetch the transitive dependencies but not bind them in the final JAR
* added a Bash script to re-package Undertow and dependencies as modules locally!
* provided INSTALL.md

## Testing

Goal: get some coverage statistics and assert functionality doesn't break in the future

* added Unit tests
* added Hamcrest as dependency

# 1.0.2
## Bind SQLite

Goal: GET request for past month's sums of trips per means of transportation as well as transportation types

* add and implement a more usable set of the records
** transportation
** past month statistics
* read them from a SQLite database
* database path comes from execution environment/ application context

-> install database renamed to setup database
-> database setup pre-fills past months with random data
-> add contract for repositories and registry to API
-> app uses ServiceLoader to obtain a repository registry
-> app instruments server with repository registry/ application context
-> app needs common-cli

(i) uses a statically initialized database to read only


