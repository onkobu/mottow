# JBoss not modular

All of the JBoss dependencies used in this project are plain (multi release) JARs. To make them module a module-info.java would be necessary, compiled and packaged by the release process of the responsible project. JARs without a module-info turn into so called automatic modules when used in a modular environment.

So it is possible to compile successfully and even use them on the classpath of a modular project like this. Start up requires Maven and a well defined Java environment, e.g. a developer's machine. It is not possible to package all this with `jlink`. This tool refuses to work with automatic packages.

A sick trick enables re-packaging a known JAR with the help of `jdeps`. Since every automatic module can be used on the classpath through ad-hoc generation of module information this can also be persisted.

# Script to the rescue

A script under mottow-undertow-mod re-packages the necessary locally available JARs. To successfully package this application:

1. `mvn clean verify` – fetch all the dependencies to your local repository
1. `mottow-undertow-mod/undertow-modular.sh`  – runs the tools in the appropriate directories
1. `mvn package` – wrap it in a Java module ZIP

Step #2 is only necessary once per bill of material (BOM). The JARs are modified permanently. Only if the BOM changes the script must be re-run so the new dependencies get re-packaged, too.