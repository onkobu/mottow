# MottoW – Means of transport to work

As a company's data scientist I want to know which means of transports are used by employees to which extent to get to work so I can draw conclusions how sustainable the work force is.

As a data protectionist I want statistics of employees to be as neutral as possible to avoid tracking of behavior.

As an employee I want to provide my means of transportation to work with minimal effort by using a fixed type and range. Only very few times I want to override this with a special type and/ or range.

# Testimonials

> As a mother of three I cannot spend much time in front of the computer. Mottow does everything on its own. HR (human resources department) set up my profile and I don't need to login in nor click anychting.

-- Mother of three

> I am a perfectionist and I like waves. With Mottow I can play while commuting to move in wavy patterns. You can't imagine how much fun it is to walk an exact kilometer so my range fits perfectly into Mottow's metric system.

-- Collegue sorting his and (secretly) others treats

> I go to work on a horse. Everybody does here in Texas. The only thing I don't like about mottow is the metric system. Can't get used to that. Despite that my employer added horse as means of transport. Nothing more sustainable in the world.

-- Cowgirl with grayish hat

> After my soccer training and even between two half-times I learned a lot about how simple REST services can be. I have no time for the entire Spring Boot stack. Mottow provides multiple endpoints with 1k lines of code.

-- (female) professional soccer player, forward position

> In 2022 it seems as if the only state-of-the-art software is an app that tracks all sort of data and sell this to anybody. As a data protectionist I'm really pleased that mottow doesn't even require a login. It runs in a regular browser. And if an employee does not consent nor login statistics are still usable based on meaningful assumptions.

-- German Datenschützer with an adorable Rottweiler

> Working for a big cloud company I am a bit disappointed about two things: this software has a level of code quality and functionality our own products don't have yet. And it is licensed in a counter productive way. We'd really appreciate making money from this without paying anything. In various meetings I suggested to make an agreement with the author. But the upper management knows the global sourcing couterfeit service will be up and running next year. They'll put more managers on the payroll and simply hit the dead horse harder.

-- Magician/ sorcerer (studied philosophy generating no income in the land of the free, talked herself into a project manager role somehow related to computers)

Sub topics:

* [Changelog](CHANGELOG.md)
* [To do](TODO.md)

# Implications from the use cases

* only for the current month trips are recorded with a user reference
* at the end of the month a report sums up the month's trips per means of transport and truncates the month table
* users get a default profile containing a single means of transport and a range in km for the shortest route from home address to working place

# Interfaces to other systems

* a directory service for authentication
* a source of public holidays and other rules to distinguish working days from non-working days
* a source of employees absence

# How to run it

Setup a database file sample.db (requires sqlite to be installed):

```
manage-db.sh sample.db
```

Fill sample.db with random data:

```
./test/fill_current_month.sh sample.db
```

Build the server (requires a JDK 17 toolchain as well as Maven 3.8):

```
mvn clean install
```

Then start the server and point it to sample.db. Open a browser and point it to http://localhost:8080/hello.

# How it basically works

## The default trip record

* does not exist as a row in the database
* a regular working day without absence of the employee is an implicit trip record
* determined by the user's profile

## The special trip record

* employee chooses at least a single day
* employee then causes insertion of at least one trip record for the day selected...
* ...to override range and/ or means of transport

It comes in handy to fill out the form with the employee's default means of transportation and range.

## The monthly report

* after the last day of a month...
* for every employee...
* and for every day of the previous month...
* if the day was a working day...
* and the employee wasn't absent
* sum the employee's trip length to and from work to the means of transport's total travel range

Wherease the employee's trip length is:

* either the entry for this particular day and employee from the set of trip records
* or the employee's default means of transport and default range from the user profile

Writes one row per means of transport and month into the month statistics table.

And what about homeoffice? Currently there is no special handling. As a workaround use a transportation named homeoffice with a range of 1. Summing this up per day and employee gives the number of days in homeoffice for all employees. This could also be the default for user profiles.

# Protection of personal data

* absence is not recorded in the database but only added temporarily for statistics
* absence is not displayed per employee but only as total
* employee's exact route is unknown, a range is sufficient
* no other data except a user name and a range is stored
* per-user trips are stored only for the current month
* the monthly report clears all per-user trips and writes per-transportation month records
* if an employee leaves only the user profile must be removed
