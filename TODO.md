# Working one-pager – WIP

* only index.html left
* navigation based on Bootstrap tabs (Bootstrap 4)
* content replaced dynamically
* CORS and security headers working, checksums/ hashes in CSS/ JS -> will break loading files locally since it forces CORS requests

Security headers:
* https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP, self für HTML/ CSS/ JS, none für REST
* Strict-Transport-Security: max-age=63072000; includeSubDomains, für 2 Jahre im Browser speichern
* Referrer-Policy: no-referrer
* X-Frame-Options: deny
* X-Content-Type-Options: nosniff
* X-Permitted-Cross-Domain-Policies: none
* Access-Control-Allow-Origin (response)

# Current Month's sum as overview

Goal: sum stored trips per transportation type.

* a (database) view filtering and grouping trips of current month
* only the trips in the database

# User profile and trips

Goal: In addition to a user profile a logged in user can view his/ her trips

* new secured endpoint, similar to /userprofile
* returns all trips for the current user

# Reporting

Goal: fill in the gaps of the already stored trips and also throw in users' defaults that didn't record anything.

* current month grouped by transportation
** current_month_trips
** user profile default values if not in trips
* until yesterday (todays trips incomplete)
* basic calendar with Easter, Christmas and New Years Day =absence
* construct a month holding working days only
* project all trips onto the working days
* fill in gaps per user with default from profile

-> each working day holds a list of transportation records, one per employee
-> for easier counting a special transportation type acts as placeholder in case of absence, range is 0

# Editing of user profile

Goal: User can edit his/ her profile, select a default transport and change the default range.

# Creating/ Editing/ Deleting trips

Goal: User can create, edit or delete trips (of the current month).

# Security Next Level

Goal: password history, showcase of enforcing rules

