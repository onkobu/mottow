#!/bin/bash

function print_usage {
cat <<EOF

    $(basename "${BASH_SOURCE[0]}") <db-file>

Inits or migrates a database file. If the file does not exist a full setup takes
place. In all other cases scripts not yet recorded in schema table are read
into the database.

EOF
	exit 0
}

if [ $# -ne 1 ] || [ "$1" = '-h' ] || [ "$1" = '--help' ]; then
	print_usage
fi

dbFile=$1

function full_setup {
	echo "Full setup of non-existing file $dbFile"

	for sqlFile in $(ls -1 sql/*.sql); do
		echo "$sqlFile"
		sqlite3 $dbFile <$sqlFile
		errCode=$?
		if [ $errCode -ne 0 ]; then
			echo "DB setup failed"
			exit $errCode
		fi
		fName=$(basename $sqlFile)
		sqlite3 $dbFile "INSERT INTO mottow_schema (schema_script) VALUES ('$fName');"
	done

	echo "Full setup successful. Now you can use $dbFile as Mottow database."
}

function already_installed {
	fName=$(basename $1)
	# there is no faster way except map/ associative array (or sub shells)
	for item in "${scripts[@]}"; do
			[[ "${item}" == "${fName}" ]] && return 0
	done
	return 1
}

if [ ! -f $dbFile ]; then
	full_setup
	exit 0
fi

readarray -t scripts < <(sqlite3 $dbFile "SELECT schema_script FROM mottow_schema ORDER BY schema_when;")
errCode=$?

if [ $errCode -ne 0 ]; then
	echo "$dbFile does not contain table schema_script"
	full_setup
	exit 0
fi

echo "DB file $dbFile exists, attempting migration"

for sqlFile in $(ls -1 sql/*.sql); do
	echo -n "$sqlFile "
	
	if already_installed $sqlFile; then
		echo "skipped"
		continue
	fi

	sqlite3 $dbFile <$sqlFile
	errCode=$?
	if [ $errCode -ne 0 ]; then
		echo "DB setup failed"
		exit $errCode
	fi
	echo "OK"
done

echo "All good. Maybe fill in some test data with test/fill_current_month $dbFile ?"
