package de.oftik.mottow.api;

/**
 * Application state.
 *
 * @author onkobu
 *
 */
public interface ApplicationContext {
    /**
     *
     * @return URL for the data source to be used.
     */
    String jdbcURL();
}
