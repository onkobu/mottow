package de.oftik.mottow.api;

/**
 * Result of an authentication attempt. Correct credentials yield a successful
 * authentication. All other attemps also return an authentication but it
 * signals an invalid/ not authenticated user.
 *
 * @author onkobu
 *
 */
public record Authentication(String userId, boolean authenticated) {
}
