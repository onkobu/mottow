package de.oftik.mottow.api;

/**
 * Provides all the different data access objects (DAO).
 *
 * @author onkobu
 *
 */
public interface DataAccessObjects extends Service {
	/**
	 *
	 * @return DAO for transports.
	 */
	Transports transports();

	/**
	 *
	 * @return DAO for month statistics.
	 */
	MonthStatistics monthStatistics();

	/**
	 *
	 * @return DAO to access user profiles.
	 */
	UserProfiles userProfiles();
}