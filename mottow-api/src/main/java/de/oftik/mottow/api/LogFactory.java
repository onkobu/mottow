package de.oftik.mottow.api;

import java.util.logging.Logger;

/**
 * Convencience class.
 *
 * @author onkobu
 *
 */
public final class LogFactory {
    private LogFactory() {
        // not to be instantiated
    }

    /**
     *
     * @param cls Class to create a logger for.
     * @return Logger named according to given class.
     */
    public static Logger getLog(final Class<?> cls) {
        return Logger.getLogger(cls.getName());
    }
}
