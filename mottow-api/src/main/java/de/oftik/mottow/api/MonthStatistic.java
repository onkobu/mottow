package de.oftik.mottow.api;

import java.time.LocalDate;

/**
 * Range for one month and a single transport type.
 *
 * @author onkobu
 * @param firstOfMonth  First day of the month.
 * @param transportId   Reference to a {@link Transport}.
 * @param transportName Transport name.
 * @param range         Range in km.
 */
public record MonthStatistic(LocalDate firstOfMonth, long transportId, String transportName, long range) {

}
