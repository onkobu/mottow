package de.oftik.mottow.api;

import java.util.Collection;

/**
 * DAO for month statistics.
 *
 * @author onkobu
 *
 */
public interface MonthStatistics {
	/**
	 *
	 * @return All available statistics.
	 */
	Collection<MonthStatistic> listAll();

	/**
	 *
	 * @return Explicitely written for current month.
	 */
	Collection<MonthStatistic> listRecordedOfCurrentMonth();
}
