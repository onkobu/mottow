package de.oftik.mottow.api;

/**
 * Distance to travel in km.
 *
 * @author onkobu
 *
 */
public record Range(int km) {

}
