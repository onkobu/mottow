package de.oftik.mottow.api;

/**
 * A well defined unit taking input to turn it into output.
 *
 * @author onkobu
 *
 */
public interface Service {
	/**
	 * State after initialization.
	 *
	 * @return Service state never <code>null</code>.
	 * @param ctx The application context to take into consideration.
	 */
	ServiceState init(ApplicationContext ctx);
}
