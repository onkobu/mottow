package de.oftik.mottow.api;

/**
 * A service' state after initiating it.
 *
 * @author onkobu
 *
 */
public enum ServiceState {
	/**
	 * Application cannot use the service, something is totally broken. The service
	 * itself wrote something to the log.
	 */
	UNUSABLE,

	/**
	 * Something outside Mottow that has to be there isn't.
	 */
	DEPENDENCY_MISSING,

	/**
	 * The service would work if the configuration were correct. User must change
	 * the configuration as stated by the service. Application must be restarted
	 * afterwards.
	 */
	CONFIGURATION_ERROR,

	/**
	 * Up and running fine.
	 */
	WORKING;

	public ServiceState and(final ServiceState other) {
		if (other.ordinal() < ordinal()) {
			return other;
		}
		return this;
	}
}
