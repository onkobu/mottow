package de.oftik.mottow.api;

/**
 * A single means of transportation, e.g. bus, train, car or by foot.
 *
 * @author onkobu
 * @param name Unique transport name.
 */
public record Transport(String name) {

}
