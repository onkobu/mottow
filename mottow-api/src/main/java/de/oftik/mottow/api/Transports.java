package de.oftik.mottow.api;

import java.util.Collection;

/**
 * DAO for transports.
 *
 * @author onkobu
 *
 */
public interface Transports {
    /**
     *
     * @return All available means of transportation.
     */
    Collection<Transport> listAll();
}
