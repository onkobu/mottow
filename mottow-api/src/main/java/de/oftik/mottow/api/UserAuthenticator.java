package de.oftik.mottow.api;

/**
 * A service responsible for turning credentials into an authentication.
 *
 * @author onkobu
 *
 */
public interface UserAuthenticator extends Service {

	/**
	 * Turns credentials into an authentication.
	 *
	 * @param creds What was transferred to the server.
	 * @return What the server allows.
	 */
	Authentication authenticate(UserCredentials creds);
}
