package de.oftik.mottow.api;

/**
 * Values transferred to the server. This can not be a record due to the state
 * change after authentication took place destroying the secret.
 *
 * @author onkobu
 *
 */
public interface UserCredentials {
	/**
	 * The unique value for the user.
	 *
	 * @return Some characters.
	 */
	String userId();

	/**
	 * The user secret.
	 *
	 * @return Some characters.
	 */
	String secret();

	/**
	 * Invoked after the authentication took place to destroy all values.
	 */
	void destroy();
}
