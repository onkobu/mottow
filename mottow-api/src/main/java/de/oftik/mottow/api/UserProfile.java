package de.oftik.mottow.api;

/**
 * A user profile.
 *
 * @author onkobu
 *
 */
public record UserProfile(String name, Transport defaultTransport, Range range) {

}
