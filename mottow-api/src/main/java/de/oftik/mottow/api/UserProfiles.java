package de.oftik.mottow.api;

/**
 * DAO to access user profiles.
 *
 * @author onkobu
 *
 */
public interface UserProfiles {
	/**
	 * Handle with care this is personal data.
	 *
	 * @param uid User id to query for.
	 * @return User profile or <code>null</code> if it does not exist.
	 */
	UserProfile getProfile(String uid);
}
