/**
 * API base package. With less than 10 classes sub packages do not make sense
 * yet.
 */
package de.oftik.mottow.api;
