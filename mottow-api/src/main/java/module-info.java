/**
 * Abstract programming interface usable by anybody.
 *
 * @author onkobu
 *
 */
module mottow.api {
    requires transitive java.logging;

    exports de.oftik.mottow.api;
}