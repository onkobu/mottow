package de.oftik.mottow.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class InterfaceApiTest {
	// Using the classloader (Thread's context class loader, system class loader,
	// record's class loader) does not return all the classes. And using a third
	// party library is no option for this isolated use case
	@ParameterizedTest
	@ValueSource(classes = { ApplicationContext.class, DataAccessObjects.class, MonthStatistics.class, Service.class,
			Transports.class, UserAuthenticator.class, UserCredentials.class })
	void allInterfacesSane(final Class<?> recordCls) {
		assertTrue(recordCls.isInterface());
	}

	// DataAccessObjects must return interfaces only.
	@Test
	void dataAccessObjectsSane() {
		assertThat(Arrays.stream(DataAccessObjects.class.getDeclaredMethods())
				.filter(m -> !m.getReturnType().isInterface()).collect(Collectors.toList()), Matchers.empty());
	}
}
