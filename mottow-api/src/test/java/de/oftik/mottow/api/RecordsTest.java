package de.oftik.mottow.api;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class RecordsTest {
	@ParameterizedTest
	@ValueSource(classes = { Authentication.class, MonthStatistic.class, Range.class, Transport.class,
			UserProfile.class })
	void allRecordsSane(final Class<?> recordCls) {
		assertTrue(recordCls.isRecord());
	}
}
