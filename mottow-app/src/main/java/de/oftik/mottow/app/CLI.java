package de.oftik.mottow.app;

import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import de.oftik.mottow.api.LogFactory;
import de.oftik.mottow.app.Mottow.MottowContext;

final class CLI {
	private static final Logger LOG = LogFactory.getLog(CLI.class);
	private static final Options OPTIONS = CliOption.createOptions();

	private final CommandLine cli;

	CLI(final String[] args) {
		final CommandLineParser p = new DefaultParser();
		CommandLine tmpCli = null;
		try {
			tmpCli = p.parse(OPTIONS, args, true);
		} catch (ParseException e) {
			LOG.throwing(CLI.class.getName(), null, e);
			cli = null;
			return;
		}
		cli = tmpCli;
	}

	boolean hasToExit(final MottowContext ctx) {
		if (cli == null || CliOption.help.isPresent(cli) || !cli.getArgList().isEmpty()) {
			if (!cli.getArgList().isEmpty()) {
				System.out.println("Unrecognized options: " + cli.getArgList());
			}
			printHelp();
			return true;
		}
		if (CliOption.version.isPresent(cli)) {
			printVersion();
			return true;
		}
		CliOption.jdbc.ifValuePresent(cli, ctx::jdbcURL, null);
		CliOption.verbose.ifPresent(cli, ctx::verbose, null);
		return false;
	}

	/**
	 * In case of <code>--help</code>.
	 */
	private static void printHelp() {
		final HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("de.oftik.mottow.app.Mottow", OPTIONS);
	}

	/**
	 * In case of <code>--version</code>.
	 */
	private static void printVersion() {
		System.out.println("\tMeans of transports to work\n\n");
		System.out.println("Version: " + Mottow.class.getPackage().getImplementationVersion());
		System.out.println("\n--help or -h for help\n");
	}

}
