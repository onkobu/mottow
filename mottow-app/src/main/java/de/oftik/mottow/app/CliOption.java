package de.oftik.mottow.app;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Option.Builder;
import org.apache.commons.cli.Options;

enum CliOption {
	help("h", false, "Print this help and exit"),

	jdbc("d", true, "JDBC URL"),

	version("v", false, "Print version and exit"),

	verbose(null, false, "Increase log verbosity");

	private final String shortOption;
	private final boolean argument;
	private final String description;
	private final Option option;

	CliOption(final String shortOption, final boolean argument, final String description) {
		this.shortOption = shortOption;
		this.argument = argument;
		this.description = description;
		option = toOption();
	}

	boolean isPresent(final CommandLine cli) {
		return cli.hasOption(shortOption);
	}

	Option toOption() {
		final Builder b;
		if (shortOption == null) {
			b = Option.builder();
		} else {
			b = Option.builder(shortOption);
		}

		return b.longOpt(name()).desc(description).hasArg(argument).build();
	}

	static Options createOptions() {
		final Options options = new Options();
		for (final CliOption o : values()) {
			options.addOption(o.option);
		}
		return options;
	}

	<T> Optional<T> ifValuePresent(final CommandLine cli, final Function<String, T> fct, final Runnable alternative) {
		if (cli.hasOption(option)) {
			return Optional.ofNullable(fct.apply(cli.getOptionValue(option)));
		}

		if (alternative != null) {
			alternative.run();
		}
		return Optional.empty();
	}

	void ifPresent(final CommandLine cli, final Consumer<Boolean> cons, final Runnable alternative) {
		// Safely trigger alternative afterwards and only if this is not true
		if (cli.hasOption(option)) {
			cons.accept(cli.hasOption(option));
			return;
		}

		if (alternative != null) {
			alternative.run();
		}
	}

	void ifPresent(final CommandLine cli, final Runnable fct, final Runnable alternative) {
		if (cli.hasOption(option)) {
			fct.run();
			return;
		}

		if (alternative != null) {
			alternative.run();
		}
	}

	String shortOption() {
		return shortOption;
	}

	String longOption() {
		return name();
	}
}