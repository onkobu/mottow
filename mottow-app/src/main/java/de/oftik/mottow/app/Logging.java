package de.oftik.mottow.app;

import java.io.IOException;
import java.util.logging.LogManager;

final class Logging {
	private Logging() {
		// not to be instantiated
	}

	static void initLogging() {
		try {
			LogManager.getLogManager().readConfiguration(Mottow.class.getResourceAsStream("/log.properties"));
		} catch (final IOException ex) {
			System.out.println("WARNING: Could not open configuration file");
			System.out.println("WARNING: Logging not configured (console output only)");
		}
	}
}
