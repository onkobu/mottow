package de.oftik.mottow.app;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Logger;

import de.oftik.mottow.api.ApplicationContext;
import de.oftik.mottow.api.DataAccessObjects;
import de.oftik.mottow.api.LogFactory;
import de.oftik.mottow.api.ServiceState;
import de.oftik.mottow.api.UserAuthenticator;
import de.oftik.mottow.server.MottowServer;

/**
 * Ties all parts, bootstraps context and hands over control to built-in server.
 *
 * @author onkobu
 *
 */
public final class Mottow {
	private static final Logger LOG = LogFactory.getLog(Mottow.class);

	static class MottowContext implements ApplicationContext {
		private String jdbcURL;
		private boolean verbose;

		@Override
		public String jdbcURL() {
			return jdbcURL;
		}

		public MottowContext jdbcURL(final String url) {
			jdbcURL = url;
			return this;
		}

		public MottowContext verbose(final boolean flag) {
			verbose = flag;
			return this;
		}

		public boolean isVerbose() {
			return verbose;
		}
	}

	// be optimistic and update through side effects when loading services
	private ServiceState finalState = ServiceState.WORKING;
	private final AtomicReference<DataAccessObjects> daoRef = new AtomicReference<>();
	private final AtomicReference<UserAuthenticator> authRef = new AtomicReference<>();

	// not to be instantiated externally
	private Mottow(final MottowContext ctx) {
		loadServices();
		if (finalState != ServiceState.WORKING) {
			LOG.severe(() -> String.format("Context initialization failed, final state is %s. Cannot start server.",
					finalState));
			return;
		}
		weaveAndStart(ctx);
	}

	// load each service independently which also modifies finalState and collect
	// all errors not only the first. This implies services are isolated from each
	// other. Initialization and weaving services happens later.
	private void loadServices() {
		final var daoOrStatus = Services.loadServiceOrUnusable(DataAccessObjects.class);
		daoOrStatus.either(daoRef::set).or(state -> {
			finalState = finalState.and(state);
		});

		final var authenticatorOrStatus = Services.loadServiceOrUnusable(UserAuthenticator.class);
		authenticatorOrStatus.either(authRef::set).or(state -> {
			finalState = finalState.and(state);
		});
	}

	// Initialization and weaving happens here. Services are not independent
	// anymore. The first error kills all following steps.
	private void weaveAndStart(final ApplicationContext ctx) {
		LOG.info("Initiating services with context");
		thenStart(onlyIfWorking(finalState -> {
			LOG.info(() -> String.format("Using service DAO %s", daoRef.get().getClass().getName()));
			return finalState.and(daoRef.get().init(ctx));
		}, finalState -> {
			LOG.info(() -> String.format("Using service UserAuthenticator %s", authRef.get().getClass().getName()));
			return finalState.and(authRef.get().init(ctx));
		}), () -> {
			LOG.info("Starting server");
			new MottowServer(daoRef.get(), authRef.get()).start();
			return null;
		}).accept(finalState);
	}

	// Only step forward to next if prev finished in working state
	private static Function<ServiceState, ServiceState> onlyIfWorking(final Function<ServiceState, ServiceState> prev,
			final Function<ServiceState, ServiceState> next) {
		return finalState -> {
			final var pState = prev.apply(finalState);
			if (pState == ServiceState.WORKING) {
				next.apply(pState);
			}
			return pState;
		};
	}

	// only invoke the terminal operation if the previous left system in working
	// condition
	private static Consumer<ServiceState> thenStart(final Function<ServiceState, ServiceState> prev,
			final Callable<Void> terminalOperation) {
		return state -> {
			if (prev.apply(state) == ServiceState.WORKING) {
				try {
					terminalOperation.call();
				} catch (Exception e) {
					LOG.throwing(Mottow.class.getName(), "server start", e);
				}
			}
		};
	}

	/**
	 * Get it up and running.
	 *
	 * @param args Command line arguments
	 * @throws Exception
	 */
	public static void main(final String[] args) throws Exception {
		Logging.initLogging();
		final var ctx = new MottowContext();
		if (new CLI(args).hasToExit(ctx)) {
			System.exit(0);
		}

		new Mottow(ctx);
	}

}
