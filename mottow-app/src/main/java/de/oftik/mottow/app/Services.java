package de.oftik.mottow.app;

import java.util.ServiceLoader;
import java.util.logging.Logger;

import de.oftik.kehys.keijukainen.function.ConsumeEither;
import de.oftik.kehys.keijukainen.function.EitherOr;
import de.oftik.mottow.api.LogFactory;
import de.oftik.mottow.api.ServiceState;

public class Services {
	private static final Logger LOG = LogFactory.getLog(Mottow.class);

	static <S> ConsumeEither<S, ServiceState> loadServiceOrUnusable(final Class<S> serviceClass) {
		return EitherOr.with(ServiceLoader.load(serviceClass).findFirst(), () -> {
			LOG.severe(() -> String.format("Could not find a implementation of %s", serviceClass.getSimpleName()));
			return ServiceState.UNUSABLE;
		});
	}

}
