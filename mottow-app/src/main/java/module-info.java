module mottow.app {
	requires commons.cli;
	requires mottow.server;
	requires mottow.api;
	requires mottow.sqlite;
	requires kehys.keijukainen;

	uses java.sql.Driver;
	uses de.oftik.mottow.api.DataAccessObjects;
	uses de.oftik.mottow.api.UserAuthenticator;
}