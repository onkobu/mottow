package de.oftik.mottow.app;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import de.oftik.mottow.app.Mottow.MottowContext;

class CLITest {

	@Test
	void nullContextNPE() {
		assertThrows(NullPointerException.class, () -> {
			new CLI(null).hasToExit(null);
		});
	}

	@Test
	void nullSafe() {
		assertFalse(new CLI(null).hasToExit(new MottowContext()));
	}

	@Test
	void showVersionWillExit() {
		final var ctx = new MottowContext();
		assertTrue(new CLI(new String[] { "-" + CliOption.version.shortOption() }).hasToExit(ctx));
		assertFalse(ctx.isVerbose());
	}

	@Test
	void unknownOptionShowsHelp() {
		final var ctx = new MottowContext();
		assertTrue(new CLI(new String[] { "-x", "--yelp" }).hasToExit(ctx));
	}

	@Test
	void jdbcUrlLong() {
		final var value = "jdbcValue";
		final var ctx = new MottowContext();
		new CLI(new String[] { "--" + CliOption.jdbc.longOption(), value }).hasToExit(ctx);
		assertThat(ctx.jdbcURL(), equalTo(value));
	}

	@Test
	void jdbcUrlShort() {
		final var value = "jdbcValue";
		final var ctx = new MottowContext();
		new CLI(new String[] { "-" + CliOption.jdbc.shortOption(), value }).hasToExit(ctx);
		assertThat(ctx.jdbcURL(), equalTo(value));
	}

	@Test
	void verboseLong() {
		final var ctx = new MottowContext();
		new CLI(new String[] { "--" + CliOption.verbose.longOption() }).hasToExit(ctx);
		assertTrue(ctx.isVerbose());
	}

	@Test
	void verboseShort() {
		assertThat(CliOption.verbose.shortOption(), Matchers.nullValue());
	}
}
