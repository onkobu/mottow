package de.oftik.mottow.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.oftik.mottow.api.MonthStatistic;
import de.oftik.mottow.api.Transport;
import de.oftik.mottow.api.UserProfile;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

/**
 * Template for all handlers.
 *
 * @author onkobu
 *
 * @param <T> Payload type.
 */
abstract class AbstractHandler<T> implements HttpHandler {
	static final Gson GSON;

	static {
		final GsonBuilder gb = new GsonBuilder();
		gb.registerTypeAdapter(Transport.class, Transports.TYPE_ADAPTER);
		gb.registerTypeAdapter(MonthStatistic.class, MonthStats.TYPE_ADAPTER);
		gb.registerTypeAdapter(UserProfile.class, UserProfiles.TYPE_ADAPTER);
		GSON = gb.create();
	}

	@Override
	public void handleRequest(final HttpServerExchange exchange) throws Exception {
		if (exchange.isInIoThread()) {
			exchange.dispatch(this);
			return;
		}
		decorateHeader(exchange);
		exchange.getResponseSender().send(GSON.toJson(getPayload()));
	}

	/**
	 * Where the real work happens.
	 *
	 * @return Will be returned in the response.
	 */
	protected abstract T getPayload();

	static HttpServerExchange decorateHeader(final HttpServerExchange ex) {
		ex.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
		return ex;
	}

}
