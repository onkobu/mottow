package de.oftik.mottow.server;

import de.oftik.mottow.api.UserProfiles;
import io.undertow.security.api.SecurityContext;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

/**
 * Provides a user profile if the current user is available and was
 * authenticated successfully.
 *
 * @author onkobu
 *
 */
public class GetCurrentUserProfileHandler implements HttpHandler {

	private final UserProfiles userProfiles;

	public GetCurrentUserProfileHandler(final UserProfiles userProfiles) {
		this.userProfiles = userProfiles;
	}

	@Override
	public void handleRequest(final HttpServerExchange exchange) throws Exception {
		if (exchange.isInIoThread()) {
			exchange.dispatch(this);
			return;
		}
		final SecurityContext context = exchange.getSecurityContext();
		if (!context.isAuthenticated()) {
			exchange.setStatusCode(401);
			return;
		}
		final var acct = context.getAuthenticatedAccount();
		if (acct.getRoles().isEmpty()) {
			exchange.setStatusCode(403);
			return;
		}
		AbstractHandler.decorateHeader(exchange);
		final var name = acct.getPrincipal().getName();
		exchange.getResponseSender().send(AbstractHandler.GSON.toJson(userProfiles.getProfile(name)));
	}

}
