package de.oftik.mottow.server;

import java.util.Collection;

import de.oftik.mottow.api.MonthStatistic;

/**
 * Lists all months' statistics.
 *
 * @author onkobu
 *
 */
public final class ListMonthStatsHandler extends AbstractHandler<Collection<MonthStatistic>> {

	private final de.oftik.mottow.api.MonthStatistics monthStats;

	ListMonthStatsHandler(final de.oftik.mottow.api.MonthStatistics monthStats) {
		this.monthStats = monthStats;
	}

	@Override
	protected Collection<MonthStatistic> getPayload() {
		return monthStats.listAll();
	}
}
