package de.oftik.mottow.server;

import java.util.Collection;

import de.oftik.mottow.api.Transport;

/**
 * Lists all means of transporation.
 *
 * @author onkobu
 *
 */
public final class ListTransportsHandler extends AbstractHandler<Collection<Transport>> {

	private final de.oftik.mottow.api.Transports transports;

	ListTransportsHandler(final de.oftik.mottow.api.Transports transports) {
		this.transports = transports;
	}

	@Override
	protected Collection<Transport> getPayload() {
		return transports.listAll();
	}
}
