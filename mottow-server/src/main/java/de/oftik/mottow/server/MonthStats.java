package de.oftik.mottow.server;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import de.oftik.mottow.api.DataAccessObjects;
import de.oftik.mottow.api.MonthStatistic;
import io.undertow.server.HttpHandler;

/**
 * Binds all the endpoints for months.
 *
 * @author onkobu
 *
 */
public class MonthStats {
	static final TypeAdapter<MonthStatistic> TYPE_ADAPTER = new TypeAdapter<MonthStatistic>() {
		@Override
		public MonthStatistic read(final JsonReader reader) throws IOException {
			return null;
		}

		@Override
		public void write(final JsonWriter write, final MonthStatistic stat) throws IOException {
			write.beginObject()//
					.name("transportName").value(stat.transportName())//
					.name("transportId").value(stat.transportId())//
					.name("rangeInKm").value(stat.range())//
					.name("firstOfMonth").value(stat.firstOfMonth().format(DateTimeFormatter.ISO_DATE))//
					.endObject();
		}
	};

	private final ListMonthStatsHandler monthStatsHandler;

	private final RecordedOfCurrentMonthHandler recordedOfCurrentMonthHandler;

	/**
	 * Create with the given set of DAOs.
	 *
	 * @param daos DAOs to operate on.
	 */
	public MonthStats(final DataAccessObjects daos) {
		monthStatsHandler = new ListMonthStatsHandler(daos.monthStatistics());
		recordedOfCurrentMonthHandler = new RecordedOfCurrentMonthHandler(daos.monthStatistics());
	}

	/**
	 * Lists all months.
	 *
	 * @return Handle that lists all months.
	 */
	public HttpHandler listAll() {
		return monthStatsHandler;
	}

	public HttpHandler listRecordedOfCurrentMonth() {
		return recordedOfCurrentMonthHandler;
	}
}
