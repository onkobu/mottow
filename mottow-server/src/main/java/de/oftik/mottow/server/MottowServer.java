package de.oftik.mottow.server;

import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import de.oftik.mottow.api.Authentication;
import de.oftik.mottow.api.DataAccessObjects;
import de.oftik.mottow.api.UserAuthenticator;
import de.oftik.mottow.api.UserCredentials;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.security.api.AuthenticationMechanism;
import io.undertow.security.api.AuthenticationMode;
import io.undertow.security.handlers.AuthenticationCallHandler;
import io.undertow.security.handlers.AuthenticationConstraintHandler;
import io.undertow.security.handlers.AuthenticationMechanismsHandler;
import io.undertow.security.handlers.SecurityInitialHandler;
import io.undertow.security.idm.Account;
import io.undertow.security.idm.Credential;
import io.undertow.security.idm.IdentityManager;
import io.undertow.security.idm.PasswordCredential;
import io.undertow.security.impl.BasicAuthenticationMechanism;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.RoutingHandler;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.util.Headers;

/**
 * Server component.
 *
 * @author onkobu
 *
 */
public class MottowServer {
	private static final String SERVER_HOST = "localhost";
	private static final int HTTP_PORT = 8080;

	private final Undertow server;
	private final Transports transports;
	private final MonthStats monthStats;
	private final IdentityManager identityManager;
	private final UserProfiles userProfiles;

	/**
	 * Creates a server instance with the given DAOs.
	 *
	 * @param daos DAOs to operate with.
	 */
	public MottowServer(final DataAccessObjects daos, final UserAuthenticator authenticator) {
		transports = new Transports(daos);
		monthStats = new MonthStats(daos);
		userProfiles = new UserProfiles(daos);
		identityManager = new IdentityManager() {
			@Override
			public Account verify(final Account account) {
				// no need (yet) to re-validate accounts, once logged in/ validated it stays
				// put.
				return account;
			}

			@Override
			public Account verify(final String id, final Credential credential) {
				return toAccount(authenticator
						.authenticate(new MottowCredentials(id, ((PasswordCredential) credential).getPassword())));
			}

			@Override
			public Account verify(final Credential credential) {
				throw new UnsupportedOperationException("Cannot verify on Credentials only");
			}
		};
		server = Undertow.builder().addHttpListener(HTTP_PORT, SERVER_HOST)
				.setHandler(Handlers.path().addPrefixPath("hello", new HttpHandler() {
					@Override
					public void handleRequest(final HttpServerExchange exchange) throws Exception {
						exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
						exchange.getResponseSender().send("Hello World");
					}
				})//
						.addPrefixPath("/", Handlers.resource(
								// This must be the MottowServer.class to make it work with >JDK 9 modules.
								// System class loader is unable to access per module resources.
								new ClassPathResourceManager(MottowServer.class.getClassLoader(), "webroot")))//
						.addPrefixPath("/mottow", new RoutingHandler()//
								.get("/transports", transports.listAll())//
								.get("/months", monthStats.listAll())//
								.get("/currentmonth", monthStats.listRecordedOfCurrentMonth())//
								.get("/userprofile",
										addSecurity(userProfiles.getCurrentProfileHandler(), identityManager))))//
				.build();
	}

	/**
	 * Get the server up and running.
	 */
	public void start() {
		server.start();
	}

	// Basically taken from the examples, see
	// https://github.com/undertow-io/undertow/blob/master/examples/src/main/java/io/undertow/examples/security/basic/BasicAuthServer.java
	private static HttpHandler addSecurity(final HttpHandler toWrap, final IdentityManager identityManager) {
		HttpHandler handler = toWrap;
		handler = new AuthenticationCallHandler(handler);
		handler = new AuthenticationConstraintHandler(handler);
		final List<AuthenticationMechanism> mechanisms = Collections
				.<AuthenticationMechanism>singletonList(new BasicAuthenticationMechanism("Means of transport to work"));
		handler = new AuthenticationMechanismsHandler(handler, mechanisms);
		handler = new SecurityInitialHandler(AuthenticationMode.PRO_ACTIVE, identityManager, handler);
		return handler;
	}

	/**
	 * Maps the authentication to Undertow's account type.
	 *
	 * @param auth Authentication going in.
	 * @return Account based on authentication.
	 */
	Account toAccount(final Authentication auth) {
		return new MottowAccount(auth);
	}

	static final class MottowCredentials implements UserCredentials {
		private final String userId;
		private String secret;

		MottowCredentials(final String userId, final char[] secret) {
			super();
			this.userId = userId;
			this.secret = new String(secret);
		}

		@Override
		public String userId() {
			return userId;
		}

		@Override
		public String secret() {
			return secret;
		}

		@Override
		public void destroy() {
			secret = null;
		}
	}

	static final class MottowAccount implements Account {
		private static final Set<String> DEFAULT_ROLES = Set.of("AUTHENTICATED");
		private final MottowPrincipal principal;
		private final Set<String> roles;

		MottowAccount(final Authentication authentication) {
			super();
			this.principal = new MottowPrincipal(authentication);
			if (authentication.authenticated()) {
				roles = DEFAULT_ROLES;
			} else {
				roles = Set.of();
			}
		}

		@Override
		public Principal getPrincipal() {
			return principal;
		}

		@Override
		public Set<String> getRoles() {
			return roles;
		}
	}

	static final class MottowPrincipal implements Principal {
		private final Authentication authentication;

		MottowPrincipal(final Authentication authentication) {
			super();
			this.authentication = authentication;
		}

		@Override
		public String getName() {
			return authentication.userId();
		}
	}
}
