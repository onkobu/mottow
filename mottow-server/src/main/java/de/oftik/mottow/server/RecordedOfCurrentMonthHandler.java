package de.oftik.mottow.server;

import java.util.Collection;

import de.oftik.mottow.api.MonthStatistic;

/**
 * Lists curent months' statistics that were explicitely recorded.
 *
 * @author onkobu
 *
 */
public final class RecordedOfCurrentMonthHandler extends AbstractHandler<Collection<MonthStatistic>> {

	private final de.oftik.mottow.api.MonthStatistics monthStats;

	RecordedOfCurrentMonthHandler(final de.oftik.mottow.api.MonthStatistics monthStats) {
		this.monthStats = monthStats;
	}

	@Override
	protected Collection<MonthStatistic> getPayload() {
		return monthStats.listRecordedOfCurrentMonth();
	}
}
