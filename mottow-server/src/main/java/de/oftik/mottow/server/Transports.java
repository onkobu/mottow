package de.oftik.mottow.server;

import java.io.IOException;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import de.oftik.mottow.api.DataAccessObjects;
import de.oftik.mottow.api.Transport;
import io.undertow.server.HttpHandler;

/**
 * Binds all endpoints for transports.
 *
 * @author onkobu
 *
 */
public class Transports {
    static final TypeAdapter<Transport> TYPE_ADAPTER = new TypeAdapter<Transport>() {
        @Override
        public Transport read(final JsonReader reader) throws IOException {
            return null;
        }

        @Override
        public void write(final JsonWriter write, final Transport transport) throws IOException {
            write.beginObject()//
                    .name("name").value(transport.name())//
                    .endObject();
        }
    };

    private final ListTransportsHandler transportsHandler;

    /**
     * Create with the given set of DAOs.
     *
     * @param daos Set of DAOs to use.
     */
    public Transports(final DataAccessObjects daos) {
        transportsHandler = new ListTransportsHandler(daos.transports());
    }

    /**
     * List all transports.
     *
     * @return Handler listing all transports.
     */
    public HttpHandler listAll() {
        return transportsHandler;
    }
}
