package de.oftik.mottow.server;

import java.io.IOException;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import de.oftik.mottow.api.DataAccessObjects;
import de.oftik.mottow.api.UserProfile;

/**
 * Handler(s) for user profile.
 *
 * @author onkobu
 *
 */
public class UserProfiles {
	static final TypeAdapter<UserProfile> TYPE_ADAPTER = new TypeAdapter<UserProfile>() {
		@Override
		public UserProfile read(final JsonReader reader) throws IOException {
			return null;
		}

		@Override
		public void write(final JsonWriter write, final UserProfile profile) throws IOException {
			write.beginObject()//
					.name("name").value(profile.name())//
					.name("defaultTransport").beginObject()//
					.name("name").value(profile.defaultTransport().name()).endObject()//
					.name("defaultRangeInKm").value(profile.range().km())//
					.endObject();
		}
	};
	private final GetCurrentUserProfileHandler currentProfileHandler;

	public UserProfiles(final DataAccessObjects daos) {
		currentProfileHandler = new GetCurrentUserProfileHandler(daos.userProfiles());
	}

	/**
	 *
	 * @return Handler for a current user.
	 */
	public GetCurrentUserProfileHandler getCurrentProfileHandler() {
		return currentProfileHandler;
	}
}
