/**
 * Server module based on JBoss Undertow.
 *
 * @author onkobu
 *
 */
module mottow.server {
	// sun.misc.Unsafe in Undertow
	requires jdk.unsupported;

	requires transitive undertow.core;

	requires transitive mottow.api;
	requires com.google.gson;

	exports de.oftik.mottow.server;

	// Grant class loader access to webroot resources
	opens webroot;
	opens webroot.css;
	opens webroot.js;
}