function loadCurrentMonth() {
$.ajax({
  method: "GET",
  url: "/mottow/currentmonth",
  dataType : 'json',
  success: function( data ) {
   $('#current_month_table tbody').children().remove();
   $.each(data, function( idx, month ) {
     $('#current_month_table tbody').append('<tr id="month_'+month.firstOfMonth+'">'+
        "<td>" + month.firstOfMonth + "</td>"+
        "<td>" + month.transportName + "</td>"+
        "<td>" + month.rangeInKm +"</td>"
     );
   });
  },
  error : function() {
    console.log('error');
  }
  });
}

function loadMonths() {
$.ajax({
  method: "GET",
  url: "/mottow/months",
  dataType : 'json',
  success: function( data ) {
   $('#month_table tbody').children().remove();
   $.each(data, function( idx, month ) {
     $('#month_table tbody').append('<tr id="month_'+month.firstOfMonth+'">'+
        "<td>" + month.firstOfMonth + "</td>"+
        "<td>" + month.transportName + "</td>"+
        "<td>" + month.rangeInKm +"</td>"
     );
   });
  },
  error : function() {
    console.log('error');
  }
  });
}

function loadProfile() {
$.ajax({
  method: "GET",
  url: "/mottow/userprofile",
  dataType : 'json',
  success: function( profile ) {
     $('#forProfile').children().remove();
     
     $('#forProfile').append('<div>Name: '+
        profile.name + "</div><div>Transport: "+
        profile.defaultTransport.name + "</div><div>Range: "+
        profile.defaultRangeInKm +"</div>"
     );
  },
  error : function() {
    console.log('error');
  }
  });
}