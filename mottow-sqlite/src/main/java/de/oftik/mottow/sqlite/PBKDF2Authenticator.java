package de.oftik.mottow.sqlite;

import static de.oftik.mottow.sqlite.SQLiteDAOs.createConnection;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import de.oftik.mottow.api.ApplicationContext;
import de.oftik.mottow.api.Authentication;
import de.oftik.mottow.api.LogFactory;
import de.oftik.mottow.api.ServiceState;
import de.oftik.mottow.api.UserAuthenticator;
import de.oftik.mottow.api.UserCredentials;

/**
 * Store passwords only salted + hashed. Original source
 * https://gist.github.com/jtan189/3804290.
 *
 * @author onkobu
 *
 */
public class PBKDF2Authenticator implements UserAuthenticator {
	private static final Logger LOG = LogFactory.getLog(PBKDF2Authenticator.class);

	// Related to grub-mkpasswd-pbkdf2, which uses SHA512
	private static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA512";

	private static final int SALT_BYTES = 24;
	private static final int HASH_BYTES = 24;

	// Depends heavily on attacker's hardware to try through the stolen list of
	// hashed passwords. For this showcase with an SQLite database (that doesn't
	// provide any means of security at all) it is only an illustration. And this
	// should not be a role model of bad config being copied all over the web.
	static final int PBKDF2_ITERATIONS = 65535;

	private static final int ITERATION_INDEX = 0;
	private static final int SALT_INDEX = 1;
	private static final int PBKDF2_INDEX = 2;

	static final Pattern PATTERN = Pattern
			.compile("^" + PBKDF2Authenticator.PBKDF2_ITERATIONS + ":[a-zA-Z0-9]+:[a-zA-Z0-9]+$");

	// Will handle a separate connection so that user credentials don't end up in
	// regular DAOs.
	private Connection connection;

	@Override
	public ServiceState init(final ApplicationContext ctx) {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (final ClassNotFoundException ex) {
			LOG.throwing(SQLiteDAOs.class.getName(), "init", ex);
			return ServiceState.DEPENDENCY_MISSING;
		}

		try {
			return innerInit(createConnection(ctx.jdbcURL()));
		} catch (final SQLException e) {
			LOG.throwing(getClass().getName(), "init", e);
			return ServiceState.UNUSABLE;
		}
	}

	ServiceState innerInit(final Connection conn) {
		this.connection = conn;
		try {
			SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
		} catch (final NoSuchAlgorithmException e) {
			LOG.severe(() -> String.format(
					"PBKDF2 algorithm %s is not available for your JDK/ JRE %s - %s. Use one that provides it.",
					PBKDF2_ALGORITHM, System.getProperty("java.vendor"), Runtime.version()));
			return ServiceState.UNUSABLE;
		}

		return ServiceState.WORKING;
	}

	@Override
	public Authentication authenticate(final UserCredentials creds) {
		try (var stmt = connection
				.prepareStatement("SELECT user_name, user_credentials FROM user_profile WHERE user_name=?")) {
			stmt.setString(1, creds.userId());
			final var res = stmt.executeQuery();
			if (!res.next()) {
				return unauthenticated(creds);
			}
			return new Authentication(creds.userId(), validatePassword(creds.secret(), res.getString(2)));
		} catch (SQLException e) {
			LOG.throwing(getClass().getSimpleName(), "authenticate", e);
		}
		return unauthenticated(creds);
	}

	private Authentication unauthenticated(final UserCredentials creds) {
		return new Authentication(creds.userId(), false);
	}

	static String createHash(final String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
		final byte[] salt = randomSalt();

		return PBKDF2_ITERATIONS + ":" + toHex(salt) + ":"
				+ toHex(pbkdf2(password.toCharArray(), salt, PBKDF2_ITERATIONS, HASH_BYTES));
	}

	static byte[] randomSalt() {
		final byte[] salt = new byte[SALT_BYTES];
		final var random = new SecureRandom();
		random.nextBytes(salt);
		return salt;
	}

	static boolean validatePassword(final String password, final String goodHash) {
		if (!PATTERN.matcher(goodHash).matches()) {
			LOG.warning(() -> String.format("Expected PBKDF2 did not match pattern %s, cannot compare.", PATTERN));
			return false;
		}
		final String[] params = goodHash.split(":");

		try {
			final int iterations = Integer.parseInt(params[ITERATION_INDEX]);
			final byte[] salt = fromHex(params[SALT_INDEX]);
			final byte[] hash = fromHex(params[PBKDF2_INDEX]);
			final byte[] testHash = pbkdf2(password.toCharArray(), salt, iterations, hash.length);
			return slowEquals(hash, testHash);
		} catch (NumberFormatException ex) {
			LOG.severe(() -> String.format("At least one part of expected PBKDF2 hash could not be parsed."));
			return false;
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			// If init passed successfully, this is very unlikely
			LOG.throwing(PBKDF2Authenticator.class.getSimpleName(), "validatePassword", e);
			return false;
		}
	}

	private static byte[] pbkdf2(final char[] password, final byte[] salt, final int iterations, final int bytes)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		final var spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
		// always get a fresh copy, saving time is not a goal
		final var skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
		return skf.generateSecret(spec).getEncoded();
	}

	// NumberFormatException is handled upstream
	private static byte[] fromHex(final String hex) {
		final byte[] binary = new byte[hex.length() / 2];
		for (int i = 0; i < binary.length; i++) {
			binary[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return binary;
	}

	/**
	 * Compares two byte arrays in length-constant time. This comparison method is
	 * used so that password hashes cannot be extracted from an on-line system using
	 * a timing attack and then attacked off-line.
	 *
	 * @param a the first byte array
	 * @param b the second byte array
	 * @return true if both byte arrays are the same, false if not
	 */
	private static boolean slowEquals(final byte[] a, final byte[] b) {
		int diff = a.length ^ b.length;
		for (int i = 0; i < a.length && i < b.length; i++) {
			diff |= a[i] ^ b[i];
		}
		return diff == 0;
	}

	static String toHex(final byte[] array) {
		final BigInteger bi = new BigInteger(1, array);
		final String hex = bi.toString(16);
		final int paddingLength = (array.length * 2) - hex.length();
		if (paddingLength > 0) {
			return String.format("%0" + paddingLength + "d", 0) + hex;
		} else {
			return hex;
		}
	}
}
