package de.oftik.mottow.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteConfig.LockingMode;
import org.sqlite.SQLiteConfig.TransactionMode;
import org.sqlite.SQLiteOpenMode;

import de.oftik.mottow.api.ApplicationContext;
import de.oftik.mottow.api.DataAccessObjects;
import de.oftik.mottow.api.LogFactory;
import de.oftik.mottow.api.MonthStatistics;
import de.oftik.mottow.api.ServiceState;
import de.oftik.mottow.api.Transports;
import de.oftik.mottow.api.UserProfiles;

/**
 * Bind all DAOs.
 *
 * @author onkobu
 *
 */
public class SQLiteDAOs implements DataAccessObjects {
	private static final Logger LOG = LogFactory.getLog(SQLiteDAOs.class);

	private Connection connection;

	private final Transports transports = new SqliteTransports(() -> connection);

	private final MonthStatistics monthStatistics = new SqliteMonthStatistics(() -> connection);

	private final UserProfiles userProfiles = new SqliteUserProfiles(() -> connection);

	@Override
	public ServiceState init(final ApplicationContext ctx) {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (final ClassNotFoundException ex) {
			LOG.throwing(SQLiteDAOs.class.getName(), "init", ex);
			return ServiceState.DEPENDENCY_MISSING;
		}

		try {
			connection = createConnection(ctx.jdbcURL());
			return ServiceState.WORKING;
		} catch (final SQLException e) {
			LOG.throwing(getClass().getName(), "init", e);
			return ServiceState.UNUSABLE;
		}
	}

	@Override
	public Transports transports() {
		return transports;
	}

	@Override
	public MonthStatistics monthStatistics() {
		return monthStatistics;
	}

	@Override
	public UserProfiles userProfiles() {
		return userProfiles;
	}

	Connection getConnection() {
		return connection;
	}

	static Connection createConnection(final String jdbcURL) throws SQLException {
		final var config = new SQLiteConfig();
		config.setLockingMode(LockingMode.EXCLUSIVE);
		config.setOpenMode(SQLiteOpenMode.EXCLUSIVE);
		config.setTransactionMode(TransactionMode.IMMEDIATE);
		return DriverManager.getConnection(jdbcURL, config.toProperties());
	}
}
