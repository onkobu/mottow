package de.oftik.mottow.sqlite;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Supplier;
import java.util.logging.Logger;

import de.oftik.mottow.api.LogFactory;
import de.oftik.mottow.api.MonthStatistic;
import de.oftik.mottow.api.MonthStatistics;

/**
 * SQLite realization of month statistics DAO.
 *
 * @author onkobu
 *
 */
public class SqliteMonthStatistics implements MonthStatistics {
	private static final Logger log = LogFactory.getLog(SqliteTransports.class);

	private final Supplier<Connection> connectionProvider;

	SqliteMonthStatistics(final Supplier<Connection> connectionProvider) {
		this.connectionProvider = connectionProvider;
	}

	@Override
	public Collection<MonthStatistic> listAll() {
		final var transports = new ArrayList<MonthStatistic>();
		try (final var stmt = connectionProvider.get().createStatement()) {
			final var res = stmt.executeQuery(
					"SELECT month_first, month_range, transport_id, transport_name FROM v_month_statistics");
			while (res.next()) {
				transports.add(toEntity(res));
			}
		} catch (final SQLException e) {
			log.throwing(getClass().getName(), "listAll", e);
		}
		return Collections.unmodifiableCollection(transports);
	}

	@Override
	public Collection<MonthStatistic> listRecordedOfCurrentMonth() {
		final var transports = new ArrayList<MonthStatistic>();
		try (final var stmt = connectionProvider.get().createStatement()) {
			final var res = stmt.executeQuery(
					"SELECT month_first, month_range, transport_id, transport_name FROM v_current_month_sum");
			while (res.next()) {
				transports.add(toEntity(res));
			}
		} catch (final SQLException e) {
			log.throwing(getClass().getName(), "listRecordedOfCurrentMonth", e);
		}
		return Collections.unmodifiableCollection(transports);
	}

	private MonthStatistic toEntity(final ResultSet res) throws SQLException {
		return new MonthStatistic(res.getDate("month_first").toLocalDate(), res.getLong("transport_id"),
				res.getString("transport_name"), res.getLong("month_range"));
	}

}
