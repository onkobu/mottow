package de.oftik.mottow.sqlite;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Supplier;
import java.util.logging.Logger;

import de.oftik.mottow.api.LogFactory;
import de.oftik.mottow.api.Transport;
import de.oftik.mottow.api.Transports;

/**
 * SQLite realization of transports DAO.
 *
 * @author onkobu
 *
 */
public class SqliteTransports implements Transports {
	private static final Logger LOG = LogFactory.getLog(SqliteTransports.class);

	private final Supplier<Connection> connectionProvider;

	SqliteTransports(final Supplier<Connection> connectionProvider) {
		this.connectionProvider = connectionProvider;
	}

	@Override
	public Collection<Transport> listAll() {
		final var transports = new ArrayList<Transport>();
		try (final var stmt = connectionProvider.get().createStatement()) {
			final var res = stmt.executeQuery("SELECT * FROM transportation");
			while (res.next()) {
				transports.add(toEntity(res));
			}
		} catch (final SQLException e) {
			LOG.throwing(getClass().getName(), "listAll", e);
		}
		return Collections.unmodifiableCollection(transports);
	}

	private Transport toEntity(final ResultSet res) throws SQLException {
		return new Transport(res.getString("transport_name"));
	}

}
