package de.oftik.mottow.sqlite;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.function.Supplier;
import java.util.logging.Logger;

import de.oftik.mottow.api.LogFactory;
import de.oftik.mottow.api.Range;
import de.oftik.mottow.api.Transport;
import de.oftik.mottow.api.UserProfile;
import de.oftik.mottow.api.UserProfiles;

public class SqliteUserProfiles implements UserProfiles {
	private static final Logger LOG = LogFactory.getLog(SqliteTransports.class);

	private final Supplier<Connection> connectionProvider;

	SqliteUserProfiles(final Supplier<Connection> connectionProvider) {
		this.connectionProvider = connectionProvider;
	}

	@Override
	public UserProfile getProfile(final String uid) {
		try (var stmt = connectionProvider.get().prepareStatement(
				"SELECT u.user_name, u.user_default_range, t.transport_name FROM user_profile u JOIN transportation t ON u.user_default_transport_id=t.transport_id WHERE u.user_name=?")) {
			stmt.setString(1, uid);
			final var res = stmt.executeQuery();
			if (res.next()) {
				return new UserProfile(res.getString("user_name"), new Transport(res.getString("transport_name")),
						new Range(res.getInt("user_default_range")));
			}
			return null;
		} catch (SQLException e) {
			LOG.throwing(getClass().getName(), "getProfile", e);
		}
		return null;
	}

}
