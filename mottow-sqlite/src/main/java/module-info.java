/**
 * DAOs backed by a SQLite database.
 *
 * @author onkobu
 *
 */
module mottow.sqlite {
	requires mottow.api;
	requires transitive org.xerial.sqlitejdbc;
	requires transitive java.sql;

	uses java.sql.Driver;

	provides de.oftik.mottow.api.DataAccessObjects with de.oftik.mottow.sqlite.SQLiteDAOs;
	provides de.oftik.mottow.api.UserAuthenticator with de.oftik.mottow.sqlite.PBKDF2Authenticator;
}