package de.oftik.mottow.sqlite;

import static de.oftik.mottow.sqlite.SQLiteDAOs.createConnection;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.matchesPattern;

import java.math.BigInteger;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Random;
import java.util.TreeSet;
import java.util.logging.Logger;
import java.util.stream.Stream;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import de.oftik.mottow.api.LogFactory;
import de.oftik.mottow.api.UserCredentials;

class PBKDF2AuthenticatorTest {
	private static final Logger LOG = LogFactory.getLog(PBKDF2AuthenticatorTest.class);

	private static final Random RANDOM = new Random();
	private static final String MEMORY_JDBC_URL = "jdbc:sqlite::memory:";

	private static Connection connection;
	private final PBKDF2Authenticator auth = new PBKDF2Authenticator();

	@Test
	void roundtripAndFormat() throws Exception {
		final var hs = PBKDF2Authenticator.createHash("null");
		assertThat(hs, matchesPattern(PBKDF2Authenticator.PATTERN));
		assertThat(PBKDF2Authenticator.validatePassword("null", hs), Matchers.is(true));
	}

	@Test
	void saltIsRandom() {
		// Is going to skip duplicates
		final var numbers = new TreeSet<BigInteger>();
		final var saltsTotal = 1_000_000;
		BigInteger min = BigInteger.ZERO;
		BigInteger max = BigInteger.ZERO;
		for (int i = 0; i < saltsTotal; i++) {
			if ((i % 10_000) == 0) {
				final var curr = i;
				LOG.info(() -> String.format("Calculated %d salts of %d", curr, saltsTotal));
			}
			final var bint = new BigInteger(PBKDF2Authenticator.randomSalt());
			if (bint.compareTo(min) < 0) {
				min = bint;
			} else if (bint.compareTo(max) > 0) {
				max = bint;
			}
			numbers.add(bint);
		}
		assertThat(numbers.size(), Matchers.greaterThanOrEqualTo(saltsTotal - 5));

		final BigInteger range = max.subtract(min);
		// create 5 buckets
		final var bucketRange = range.divide(BigInteger.valueOf(5));
		final int[] counters = new int[5];
		// adjust index according to lowest range value
		final int bucketOfs = Math.toIntExact(min.divide(bucketRange).longValue());
		for (final var bint : numbers) {
			counters[Math.toIntExact(bint.divide(bucketRange).longValue()) - bucketOfs]++;
		}

		LOG.info(() -> String.format("counters: %s", Arrays.toString(counters)));

		final var smallestBucketSize = saltsTotal / 10 - 1000;
		final var mediumBucketSize = smallestBucketSize * 2;

		// Gaussian distribution
		assertThat(counters[0], Matchers.greaterThanOrEqualTo(smallestBucketSize));
		assertThat(counters[4], Matchers.greaterThanOrEqualTo(smallestBucketSize));
		assertThat(counters[1], Matchers.greaterThanOrEqualTo(mediumBucketSize));
		assertThat(counters[3], Matchers.greaterThanOrEqualTo(mediumBucketSize));
		assertThat(counters[2], Matchers.greaterThanOrEqualTo(mediumBucketSize * 2));
	}

	@BeforeAll
	public static void initDatabase() throws Exception {
		Class.forName("org.sqlite.JDBC");
		connection = createConnection(MEMORY_JDBC_URL);
		connection.createStatement()
				.execute("CREATE TABLE IF NOT EXISTS user_profile (user_name TEXT, user_credentials TEXT);");
	}

	@BeforeEach
	public void initAuthenticator() {
		auth.innerInit(connection);
	}

	@AfterAll
	public static void dropDatabase() throws Exception {
		connection.createStatement().execute("DROP TABLE IF EXISTS user_profile;");
	}

	@Test
	void dbPasswordNotAuthenticated() {
		assertThat(auth.authenticate(new TestCredentials("bimmie", "hash")).authenticated(), Matchers.is(false));
	}

	@ParameterizedTest
	@MethodSource("passwordsAndHashes")
	void grubPasswordMatches(final String pw, final String given) {
		assertThat(PBKDF2Authenticator.validatePassword(pw, given), Matchers.is(true));
	}

	static Stream<Arguments> passwordsAndHashes() {
		// grub-mkpasswd-pbkdf2 -c 65535 -s 24 -l 24, lulu
		return Stream.of(Arguments.of("lulu",
				"65535:7907F009149C0CF95CAC14FB629778ACD5437B2F6AE89FD0:FE197520B9A74E4E70CEEF5FFF124BAD224B792A3E997124"),
				Arguments.of("mottow",
						"65535:e3c4870cd444f5568a2a9a3f1aa4ecfa5189e0e085c8d937:0935ca8f0a4237afe071c710e89a2e4e71afcfd9b48b7edd"));
	}

	@Test
	void dbPasswordAuthenticated() throws Exception {
		final var userId = String.valueOf(RANDOM.nextLong());
		final var password = PBKDF2Authenticator.toHex(PBKDF2Authenticator.randomSalt());

		final var stmt = connection
				.prepareStatement("INSERT INTO user_profile (user_name, user_credentials) VALUES (?, ?)");
		stmt.setString(1, userId);
		stmt.setString(2, PBKDF2Authenticator.createHash(password));
		// no ResultSet thus false
		assertThat(stmt.execute(), Matchers.is(false));

		assertThat(auth.authenticate(new TestCredentials(userId, password)).authenticated(), Matchers.is(true));
	}

	private static class TestCredentials implements UserCredentials {
		private final String userId;
		private final String secret;

		TestCredentials(final String userId, final String secret) {
			super();
			this.userId = userId;
			this.secret = secret;
		}

		@Override
		public String userId() {
			return userId;
		}

		@Override
		public String secret() {
			return secret;
		}

		@Override
		public void destroy() {
		}
	}
}
