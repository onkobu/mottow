#!/bin/bash

# Limitations
#
# - lacks requires transitive <module>, causes warnings
# - compiles and packages with JDK 17 breaking all efforts made for dependencies to be multi release
# - cuts off logging configuration from jboss-logging, neither SLF4J nor any logging provider is going to be available

# could be any JDK as well as base (directly put into jdeps --multi-release)
#

set -Eo pipefail

MULTI_RELEASE_TARGET=17
JAVA_HOME=/opt/openjdk-bin-17/
JDEPS=$JAVA_HOME/bin/jdeps
JAVAC=$JAVA_HOME/bin/javac
JAR=$JAVA_HOME/bin/jar

function print-usage-and-exit {
cat <<EOF

    $(basename "${BASH_SOURCE[0]}") [-v]

Re-package Undertow-related JARs to make them valid modular JARs.

    -f force re-packaging

EOF
exit 0
}

if [ "$1" = '-h' ] || [ "$1" = '--help' ]; then
    print-usage-and-exit
fi
 
force=0
if [ "$1" = '-f' ]; then
    force=1
fi

JBOSS_BASE_VERSION=2.1.18.Final
JBOSS_LOGGING_VERSION=3.4.1.Final
WILDFLY_COMMON_VERSION=1.5.4.Final
WILDFLY_CONFIG_VERSION=1.0.1.Final
XNIO_API_VERSION=3.8.7.Final
JBOSS_THREADS_VERSION=3.1.0.Final
UNDERTOW_VERSION=2.2.18.Final

LOG4J_API_VERSION=2.18.0
SLF4J_API_VERSION=1.7.36

function to-explicit-module {
    jarFile=$1
    jarDir=$(dirname $1)
    jarName=$(basename $1)

    pushd $jarDir
    echo Attempting re-write of $jarName as Java module
    
    if [ $force -eq 0 ]; then
        moduleInfoFound=$(unzip -t $jarFile | grep module-info | wc -l)
        
        if [ $moduleInfoFound -gt 0 ]; then
            echo "module-info already available, skipping"
            return
        fi
    fi
    
    backupFile=${jarName}.backup
    if [ ! -f $backupFile ]; then
        echo creating a backup first
        cp $jarName $backupFile
    elif [ $force -ne 0 ]; then
        echo restoring from backup/ forced overwrite
        cp $backupFile $jarName
    fi
    
    if [ $# -eq 2 ]; then
        modulePath="$2"
    fi
    
    if [[ $jarName =~ ^wildfly-client-config ]]; then
        echo "Fixing wildfly-client-config by removing package schema to avoid collisions with xnio.api"
        zip -d $jarName schema/* schema
        zip -d $jarName schema
    fi
    
    OPTS=(--ignore-missing-deps --multi-release ${MULTI_RELEASE_TARGET} --generate-module-info .)
    
    if [ ! -z "$modulePath" ]; then
        OPTS+=(--module-path ${modulePath})
    fi
    
    # Helpful for debugging, re-directing STDOUT to files makes it even more fragile
    #$JDEPS "${OPTS[@]}" ${jarName}
    
    moduleInfoFile=$($JDEPS "${OPTS[@]}" ${jarName} | tail -n 1 | awk '{print $3}')

    if [[ $jarName =~ ^xnio-api ]]; then
        echo "Fixing module-info.java of xnio-api with uses declaration for org.xnio.XnioProvider"
        sed -i 's/}/uses org.xnio.XnioProvider;}/' $moduleInfoFile
        
        # provides in xnio-nio is not necessary it'll be auto-generated
    fi
    
    cat $moduleInfoFile
    
    echo module info $moduleInfoFile

    moduleName=$(echo $moduleInfoFile | awk -F'/' '{print $2}')

    OPTS=(--patch-module ${moduleName}=${jarName})
    if [ ! -z "$modulePath" ]; then
        OPTS+=(--module-path ${modulePath})
    fi
        
    # bails out if module-info.java is not found
    $JAVAC "${OPTS[@]}" ${moduleInfoFile}
    javacErrCode=$?
    if [ $javacErrCode -ne 0 ]; then
        echo "javac failed, no re-packaging"
        return
    fi
    
    # Majority is a multi release JAR but also provide JDK 8 as base
    # thus modular JAR for base would be invalid. Since it supposed
    # to run on JDK 17 (with jdeps fixed in JDK 14) this is the consequence
    # 
    $JAR uf ${jarName} -C $(dirname $moduleInfoFile) module-info.class
    
    errCode=$?
    if [ $errCode -ne 0 ]; then
        echo "jar command failed, no re-packaging"
        return
    fi
    
    echo "Cleaning ${moduleInfoFile}"
    rm -rf ${moduleName}
    
    if [ -f ${jarName}.sha1 ]; then
        echo "Re-writing SHA1 file"
    
        sha1sum=$(sha1sum -b $jarName | awk '{print $1}')
        echo -n "$sha1sum" >${jarName}.sha1
    fi

    popd
}

# Intentionally without quotes to resolve ~
repoHome=~/.m2/repository
baseModulePath="${repoHome}/org/slf4j/slf4j-api/${SLF4J_API_VERSION}/slf4j-api-${SLF4J_API_VERSION}.jar:${repoHome}/org/apache/logging/log4j/log4j-api/${LOG4J_API_VERSION}/log4j-api-${LOG4J_API_VERSION}.jar:${repoHome}/org/jboss/logmanager/jboss-logmanager/${JBOSS_BASE_VERSION}.Final/jboss-logmanager-${JBOSS_BASE_VERSION}.Final.jar"

jbossLoggingModule="${repoHome}/org/jboss/logging/jboss-logging/$JBOSS_LOGGING_VERSION/jboss-logging-${JBOSS_LOGGING_VERSION}.jar"
to-explicit-module ${jbossLoggingModule} # $baseModulePath

wildflyCommonModule="${repoHome}/org/wildfly/common/wildfly-common/$WILDFLY_COMMON_VERSION/wildfly-common-${WILDFLY_COMMON_VERSION}.jar"
to-explicit-module ${wildflyCommonModule} "${jbossLoggingModule}"

# should require transitive wildfly-common
jbossThreadsModule="${repoHome}/org/jboss/threads/jboss-threads/${JBOSS_THREADS_VERSION}/jboss-threads-${JBOSS_THREADS_VERSION}.jar"
to-explicit-module ${jbossThreadsModule} "${jbossLoggingModule}:${wildflyCommonModule}"

wildFlyConfigModule=${repoHome}/org/wildfly/client/wildfly-client-config/${WILDFLY_CONFIG_VERSION}/wildfly-client-config-${WILDFLY_CONFIG_VERSION}.jar
to-explicit-module ${wildFlyConfigModule} "${jbossLoggingModule}:${wildflyCommonModule}"

# should require transitive jboss-threads
xnioApiModule=${repoHome}/org/jboss/xnio/xnio-api/${XNIO_API_VERSION}/xnio-api-${XNIO_API_VERSION}.jar
to-explicit-module ${xnioApiModule} "${jbossLoggingModule}:${wildflyCommonModule}:${wildFlyConfigModule}:${jbossThreadsModule}"

xnioModule=${repoHome}/org/jboss/xnio/xnio-nio/${XNIO_API_VERSION}/xnio-nio-${XNIO_API_VERSION}.jar
to-explicit-module ${xnioModule} "${jbossLoggingModule}:${wildflyCommonModule}:${wildFlyConfigModule}:${xnioApiModule}:${jbossThreadsModule}"

undertowCoreModule=${repoHome}/io/undertow/undertow-core/${UNDERTOW_VERSION}/undertow-core-${UNDERTOW_VERSION}.jar
to-explicit-module ${undertowCoreModule} "${jbossLoggingModule}:${wildflyCommonModule}:${wildFlyConfigModule}:${xnioApiModule}:${xnioModule}:${jbossThreadsModule}"

