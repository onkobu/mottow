--
-- setup version database, records scripts already executed and
-- controls re-start
CREATE TABLE mottow_schema (
	schema_id		INTEGER		PRIMARY KEY AUTOINCREMENT,
	schema_script 	VARCHAR(100) NOT NULL,
	schema_when		DATETIME	NOT NULL DEFAULT (datetime('now','localtime')),

	UNIQUE (schema_script)
)
