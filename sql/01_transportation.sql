-- means of transportation = what brings you
-- there, e.g. bus, car, by foot
--
create table if not exists transportation (
	transport_id		INTEGER		PRIMARY KEY AUTOINCREMENT,
	transport_name		VARCHAR(50)	NOT NULL,

	unique (
		transport_name
	)
);
