-- For the showcase it is sufficient if a user has
-- a user name (that never changes) and a default
-- trip. It consists of a single type of transportation
-- with a range
--
create table if not exists user_profile (
	user_id						INTEGER		PRIMARY KEY AUTOINCREMENT,
	user_name					VARCHAR(255) NOT NULL,
	user_default_range			INTEGER NOT NULL,
	user_default_transport_id	INTEGER	NOT NULL REFERENCES transportation(transport_id),
	unique (user_name)
);
