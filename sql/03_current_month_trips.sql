-- Only the current month's records
--
create table if not exists current_month_trips (
	trip_id					INTEGER		PRIMARY KEY AUTOINCREMENT,
	trip_day				DATE		NOT NULL DEFAULT (datetime('now', 'localtime') ),
	trip_user_profile_id	INTEGER		NOT NULL REFERENCES user_profile(id),
	trip_transportation_id	INTEGER		NOT NULL REFERENCES transportation(transport_id),
	trip_range				INTEGER		NOT NULL,

	unique (
		trip_day,
		trip_user_profile_id,
		trip_transportation_id)
)
