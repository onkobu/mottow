-- A standard set of means of transportation.
-- Homeoffice is included so it can be used with range of 1
-- as default in user profiles. This enables counting employees
-- in home office.
--
insert or ignore into transportation (transport_name) VALUES
	("Bus"), ("Bahn"), ("Straßenbahn"), ("Auto"), ("Fahrrad"), ('zu Fuß'), ('Homeoffice');
