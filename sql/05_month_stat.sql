-- any past month's record split by transportation
-- This is a result of a report. Entries from current_month_trips
-- are summed groupt by transportation and put here. Finally
-- current_month_trips is emptied
--
create table IF NOT EXISTS month_statistics (
	month_id	INTEGER		PRIMARY KEY AUTOINCREMENT,
	month_first	DATE		NOT NULL,
	month_transportation_id	INTEGER NOT NULL REFERENCES transportation(transport_id),
	month_range	INTEGER,

	UNIQUE (
		month_first,
		month_transportation_id)
)
