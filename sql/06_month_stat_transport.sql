-- To make it re-issuable
DROP VIEW IF EXISTS v_month_statistics;

-- JOIN to save effort in business logic
--
CREATE VIEW v_month_statistics AS
SELECT * FROM month_statistics s JOIN transportation t
	ON s.month_transportation_id = t.transport_id;
