--
-- Never store passwords plain text. Showcases fills it
-- with PBKDF2 hashed values
--
ALTER TABLE user_profile ADD COLUMN user_credentials VARCHAR(100);
