-- Netto view of month summed up with a bit
-- of date safety in case deletion is not
-- perfect
--
DROP VIEW IF EXISTS v_current_month_sum;

CREATE VIEW IF NOT EXISTS v_current_month_sum AS
SELECT strftime('%Y-%m-','now') || '01 00:00:00' as month_first, 
       sum(trip_range) as month_range, 
       (SELECT transport_name FROM transportation WHERE transport_id=trip_transportation_id) as transport_name, 
       trip_transportation_id as transport_id
    FROM current_month_trips
    WHERE trip_day > strftime('%Y-%m-','now') || '01T00:00:00'
    GROUP BY trip_transportation_id
    ORDER BY month_range DESC;
