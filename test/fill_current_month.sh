#!/bin/bash

function print_usage {
cat <<EOF

    $(basename "${BASH_SOURCE[0]}") <db-file>

Generates and inserts test data into the given <db-file>. User profiles are
created if necessary. If they exist, they stay untouched. Some users never
get trip entries so the report must use their defaults. All other users
get random non-default entries in rare occasions (3 out of 10 days). Finally
weekends are skipped entirely.

EOF
	exit 0
}

if [ $# -ne 1 ] || [ "$1" = '-h' ] || [ "$1" = '--help' ]; then
	print_usage
fi

targetDB=$1

users=("Ronny" "Yvonne" "Mirko" "Babsi" "Marlon" "Judith" "Herbert" "Heike")

# grub-mkpasswd-pbkdf2 -c 65535 -s 24 -l 24
# omit grub.pbkdf2.sha512.
# Hash for password mottow
DEFAULT_PASSWORD="65535:e3c4870cd444f5568a2a9a3f1aa4ecfa5189e0e085c8d937:0935ca8f0a4237afe071c710e89a2e4e71afcfd9b48b7edd"

function is_user_with_trips {
	# only these two never create trips
	[[ ! "$1" =~ 'Ronny' ]] && [[ ! "$1" =~ 'Yvonne' ]] && return

	false
}

function random_range {
	shuf -i5-100 -n 1
}

function random_total_range {
	shuf -i50-1000 -n 1
}


function create_user_profile {
	userName=$1
	userTransport=$2

	exists=$(sqlite3 $targetDB "SELECT 1 FROM user_profile WHERE user_name='$userName'")

	if [ ! -z "$exists" ] && [ $exists -eq 1 ]; then
		echo "User profile $user already exists"
		return
	fi
	userRange=$(random_range)
	echo "User profile for $user: $userTransport, ${userRange}km"

	if is_user_with_trips $userName; then
		sqlite3 $targetDB "INSERT INTO user_profile (user_name, user_default_transport_id, user_default_range, user_credentials ) VALUES ('$userName', (SELECT transport_id from transportation where transport_name='$userTransport'), $userRange, '$DEFAULT_PASSWORD');"
	else
		sqlite3 $targetDB "INSERT INTO user_profile (user_name, user_default_transport_id, user_default_range ) VALUES ('$userName', (SELECT transport_id from transportation where transport_name='$userTransport'), $userRange);"
	fi
}

function create_user_trips {
	userName=$1
	date=$2

	rand=$(shuf -i1-10 -n 1)

	# prefer default transport in 7 out of 10 cases
	if [ $rand -lt 7 ]; then return; fi
	
	randomTransport=$(sqlite3 $targetDB "SELECT transport_id FROM transportation WHERE transport_id <> (SELECT user_default_transport_id FROM user_profile u WHERE user_name = '$userName') ORDER BY RANDOM() LIMIT 1;")
    range=$(random_range)

	echo "INSERT INTO current_month_trips (trip_day, trip_user_profile_id, trip_transportation_id, trip_range) VALUES ('$date',(SELECT user_id from user_profile where user_name='$userName'), $randomTransport, $range );"

	sqlite3 $targetDB "INSERT INTO current_month_trips (trip_day, trip_user_profile_id, trip_transportation_id, trip_range) VALUES ('$date',(SELECT user_id from user_profile where user_name='$userName'), $randomTransport, $range );"
}

function is_working_day {
	date=$1
	dayInWeek=$(date -d "$date" +%u)

	[[ $dayInWeek < 6 ]] && return

	false
}

function create_past_records {
	echo "Creating past month records for 12 months"

	startDate=$(date -d "-$(date +%d) days day -1 year" +%Y-%m-%d)

	for (( x=1; x<13; x++ )); do
		echo $startDate
		
		for trsp in ${transports[@]}; do
			range=$(random_total_range)
			echo -n "$trsp: $range "
			sqlite3 $targetDB "INSERT OR IGNORE INTO month_statistics (month_first, month_transportation_id, month_range) VALUES ('$startDate 00:00:00', (SELECT transport_id FROM transportation WHERE transport_name='$trsp'), $range);"
		done
		echo ""
		startDate=$(date -d "$startDate month" +%Y-%m-%d)
	done
}

function homeoffice_range_one {
    # all trips are created with a random range, homeoffice days shall be counted by using
    # a range of 1. This is just a showcase not an enforced rule
    sqlite3 $targetDB "UPDATE current_month_trips SET trip_range=1 where  trip_transportation_id=(SELECT transport_id from transportation where transport_name='Homeoffice')"
}

readarray -t transports < <(sqlite3 $targetDB "SELECT transport_name from transportation")

transportsSize=${#transports[@]}

idx=0

for user in ${users[@]}; do
	trpIdx=$((idx % transportsSize))
	create_user_profile $user ${transports[$trpIdx]}
done

# inner date returns current day which is subtracted thus last
# day of previous month...added one month
daysInMonth=$(date -d "-$(date +%d) days month" +%d)
yearAndMonth=$(date +%Y-%m-)

for (( x=1; x<=daysInMonth;x++ )); do

	day="$yearAndMonth$x"
	if ! is_working_day $day; then
		echo "Skipping non working day $day"
		continue
	fi

	for user in ${users[@]}; do
		if is_user_with_trips $user; then
			echo creating trips for $user $day
			create_user_trips $user $day
		else
			echo user $user has only default transportation
		fi
	done
	(( idx++ ))
done

create_past_records

homeoffice_range_one
